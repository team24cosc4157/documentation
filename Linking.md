## Linking between files

# WIP!

**If you have any questions or confusions, please ask your DevOps!**

---



In this guide, I may use the following shorthand:


| Shorthand     | Meaning       |
| ------------- | ------------- |
| `https://url.com` | `https://demand.team24.softwareengineeringii.com` OR `https://supply.team24.softwareengineeringii.com` (for cases in which they will work in exactly the same way)|
| `https://demandurl.com` | `https://demand.team24.softwareengineeringii.com` |
| `https://supplyurl.com` | `https://supply.team24.softwareengineeringii.com` |


**Overview / summary**

* `/commonservices/filepath` -> https://url.com/filepath
* `/demand_frontend/filepath` -> https://demandurl.com/fe/filepath
* `/supply_frontend/filepath` -> https://supplyurl.com/fe/filepath  

* Hyperlinks (https://url.com) **must not** begin with `www.`  

* Plain URLs with no filepath after link to `index.html` pages. i.e.:
    * https://url.com -> `/commonservices/index.html`
    * https://demandurl.com/fe/
    * NOTE that it must be `/fe/` with a slash after it. https://url.com/fe will not work.

---

**Vocabulary**

* 'directory' == 'folder'
* 'directory' != 'repo' || 'repository' (but our repositories are also directories)

**Note**

To display directory structures, this guide will be using the following notation:

```
parent_directory/
├── child_directory/
│   ├── file_1
│   └── file_2
├── child_directory_2/
│   ├── file_3
│   └── file_4
├── file_5
└── file_6
```

---

#### Within the same repo

Example directory structure:
```
home/team24/
└── demand_frontend/
    ├── index.html
    ├── other_page.html
    ├── css/
    │   └── index.css
    └── js/
        └── javascript.js
```
1. **Rule:**
    To link to a file within the same directory (and not in a child directory), use `"./filename"`.

    **Example:**
    Inside of `index.html`, you want to link to `other_page.html`. You would use:
    `"./other_page.html"`

2. **Rule:**
    To link to a file within a child directory of the same directory, use `"./child_directory/filename"`.

    **Example:**
    Inside of `index.html`, you want to link to `index.css`. You would use:
    `"./css/index.css"`

3. **Rule:**
    To link to a file in a parent directory, use `"../filename"`.

    **Example:**
    Inside of `index.css`, you want to link to `index.html`. You would use:
    `"../index.html"`

4. **Rule:**
    To link to a file in a different child directory of the parent directory, use `"../child_directory/filename"`.

    **Example:**
    Inside of `index.css`, you want to link to `javascript.js` (for some reason). You would use:
    `"../js/javascript.js"`

---

**Linking between different repositories in the same server and linking between repositories in different servers works exactly the same.**
You just need to use the URL of the file you are trying to link to.

#### Determining a file's URL



**If you want to, you can use the URL everywhere, including to link between files in the same repo.**

---

#### In different repos

Example directory structure:
```
home/team24/
├── commonservices/
│   └── commonservices_page.html
└── demand_frontend/
    └── frontend_page.html

```

* **Rule:**
    To link from a file in the front end repository to a file in the common services repository within the same server, use `"https://url.com/filepath"`.

    **Example:**
    Inside of `frontend_page.html`, you want to link to `commonservices_page.html`. You would use:
    `"https://demand.team24.com/commonservices_page.html"`

* **Rule:**
    To link from a file in the commmon services repository to a file in the front end repository within the same server, use `"https://url.com/fe/filepath"`. Note the **`/fe/`**.

    **Example:**
    Inside of `commonservices_page.html`, you want to link to `frontend_page.html`. You would use:
    `"https://demand.team24.com/fe/frontend_page.html"`

___

#### In different servers

Example directory structure:

**Demand-side server**
```
home/team24/
├── commonservices/
│   └── commonservices_page.html
└── demand_frontend/
    └── demand_frontend_page.html
```
**Supply-side server**
```
home/team24/
├── commonservices/
│   └── commonservices_page.html
└── supply_frontend/
    └── supply_frontend_page.html
```

* Inside of `demand_frontend_page.html`, you want to link to `supply_frontend_page.html`. You would use:

    `"https://supply.team24.softwareengineeringii.com/fe/supply_frontend_page.html"`

* Inside of `supply_frontend_page.html`, you want to link to `demand.html`. You would use:

    `"https://demand.team24.softwareengineeringii.com/fe/demand_frontend_page.html"`

* Inside of `demand_frontend_page.html`, you want to link to `commonservices_page.html` from the supply-side server.
    You shouldn't need to do this. The `commonservices` directory on each server should be identical to the directory on the other server.



___

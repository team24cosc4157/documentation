# Directory

* [Coding style guide](https://bitbucket.org/team24cosc4157/documentation/src/master/CodingStyleGuide.md)

* [Linking between files](https://bitbucket.org/team24cosc4157/documentation/src/master/Linking.md)

* [Linux commands, Git commands, and DevOps notes](https://bitbucket.org/team24cosc4157/documentation/src/master/Commands.md)

* [Naming conventions](https://bitbucket.org/team24cosc4157/documentation/src/master/NamingConventions.md)

* [UI Style guide](https://bitbucket.org/team24cosc4157/documentation/src/master/Style.md)

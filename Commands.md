## Linux commands, Git commands, and other notes from your DevOps

# WIP!

---

**Operating systems**

* This documentation is primarily written for Linux and Mac command line prompt. Windows instructions _may_ differ.

**Vocabulary**

* 'directory' == 'folder'
* 'directory' != 'repo' || 'repository' (although repositories _are_ directories)

**Tips & tricks**

* If you press `tab`, the command line interface will do its best to auto-complete any file or directory names.

---

## Using Git

### Basic instructions

* Downloading the repo

    1. If you don't yet have the repository on your local machine (laptop or PC), clone it first:

        `git clone https://[username]@bitbucket.org/team24cosc4157/[reponame]`

        The repository on Bitbucket will also have this line available for you to copy-paste.

* Working in the repo

    2. Navigate to the repository:

        `cd [reponame or filepath]`

    2. Switch to whichever branch you want to work in: see Branches section below

        * **Don't do work on master!** Verify that you're not on master using `git branch`

    3. Make sure your local repository is up to date:

        `git pull`

    2. On your machine (i.e., **not** in the command line – in Finder or File Explorer):
        * Go to the repository and edit the files you want in a text editor or IDE or
        * Add or remove files from the repository or
        * Whatever else it is you're wanting to change

* Uploading to Bitbucket

    3. Stage all changes for commit (other options in General section below):

        `git add -A`

    4. Commit changes and leave a descriptive message:

        `git commit -m '[message here]'`

    5. Push changes to Bitbucket:

        `git push`

### Commands

#### General

**Cloning a git repository**

* `git clone https://[username]@bitbucket.org/team24cosc4157/[reponame]` – Clone (download) a repo to your local machine

    The repository on Bitbucket will also have this line available for you to copy-paste.

**Uploading changes**

* `git add [filename]` – Stage a file for commit (have to do this before committing)
    * `git add -A` – Stage ALL files for commit
* `git commit -m '[message here]'` – Commit changes to be pushed and include a message with a short description of any changes
* `git push` – Push (upload) any changes from your local machine to Bitbucket

**Downloading changes**

* Check that you are on the branch you want to be on – see section below
* `git pull` – Download any changes to the local repository

#### Branches

* `git branch` – List branches
    * The active branch should have an `*` before it and may be colored differently depending on your theme
* `git branch [branchname]` – Create a new branch (you still need to `git checkout`; see below)
* `git checkout [branchname]` – Switch to a different branch that is already on your local machine (e.g. one you just made)
* `git fetch && git checkout [branchname]` – Switch to a different branch that is not on your machine yet (e.g. one another team member made)
    * Again – you can check which branch is active with `git branch`
* `git checkout -b [branchname]` – Create and checkout (switch to) a branch at the same time.
* `git branch -d [branchname]` – Delete a branch (maybe take a look at it on Bitbucket first, though)

[More info on branches](https://www.atlassian.com/git/tutorials/using-branches).

---

## General commands

#### Files

**Creating, removing, & renaming files**

* `touch [filename]` – Create a file
* `mv [oldfilename] [newfilename]` – Rename a file
* `rm -r [filename]` – Remove/delete a file

**Editing files**

* `nano [filename]` – Edit a file
    * `^X` on your keyboard – Exit the file (`^` is the `control` key)
    * It will prompt you and walk you through how to save any changes
* You can also use `vim [filename]`, but the syntax is pretty confusing so I won't get into it here. [Here](https://coderwall.com/p/adv71w/basic-vim-commands-for-getting-started) is a pretty good guide to Vim commands

#### Directories

Recall that 'directory' is just another word for folder.

**Creating & removing directories**

* `mkdir [directoryname]` – Create a new directory inside the current directory
* `rm -r [directoryname]` – Remove/delete a directory

**Navigating directories**

* `cd [directoryname]` – Open a directory
* `ls` – List files and other directories in the current directory
    * On Windows, this command is `dir`

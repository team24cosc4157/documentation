# Coding Style Guide

### Comments
* Leave a space between the comment body and any prefix or suffix.
* Do not capitalize comments.

Examples:
```
// comment like this
```

```
<!-- or like this -->
```

```
# or like this
```

---

### Beautify
Run all code through Atom (download [here](https://atom.io/)) using the [Beautify](https://atom.io/packages/atom-beautify) package (plugin).

* For **CSS**:
    * Use all default settings.
    * Use JS Beautify as the default beautifier.
* For **HTML**:
    * Use all default settings.
    * Use JS Beautify as the default beautifier.
* For **JavaScript**:
    * Use all default settings.
    * Use JS Beautify as the default beautifier.
* For **JSON**:
    * Use all default settings.
    * Use JS Beautify as the default beautifier.
* For **NginX**:
    * Use all default settings except:
        * Uncheck the 'Don't join curly brackets' option.
    * Use Nginx Beautify as the default beautifier.
* For **Python**:
    * Use all default settings.
    * Use autopep8 as the default beautifier.

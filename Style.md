# Team 24 UI Style Guide

---

## Color scheme
![Color scheme](./Images/ColorScheme.png)

| Color | Hex | RGB |
| ----- | --- | --- |
| Off-white | `#FDF6E4` | `235, 256, 228` |
| Blue | `#A7D2CB` | `167, 210, 203` |
| Yellow | `#F2D388` | `242, 211, 136` |
| Orange | `#C98474` | `201, 132, 116` |
| Purple | `#874C62` | `135, 76, 98` |

---

## Fonts

To include the following fonts in webpages, include the following line in the head section of your HTML:
```<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito|Muli">```

### Headers

Use the font **Nunito** for headers.
N.B.: Nunito != Nunito Sans

![Nunito example](./Images/NunitoExample.png)

### Body text

Use the font **Muli** for body text.

![Muli example](./Images/MuliExample.png)
